# Sample AEM project for Content Fragments Links Rewriter / Externalizer

It is intended to be used as a reference only of how links, within a Content Fragment, can be externalized by extending the AEM JSON Exporter mechanism.

This has been tested with AEM >= 6.5.7

## Modules

The main parts of the template are:

* core: Java bundle containing all core functionality like OSGi services, listeners or schedulers, as well as component-related Java code such as servlets or request filters.
* ui.apps: contains the /apps (and /etc) parts of the project, ie JS&CSS clientlibs, components, and templates
* ui.content: contains sample content using the components from the ui.apps
* ui.config: contains runmode specific OSGi configs for the project
* ui.frontend: an optional dedicated front-end build mechanism (Angular, React or general Webpack project)
* all: a single content package that embeds all of the compiled modules (bundles and content packages) including any vendor dependencies

## Files of interest

The following are the files specially added to accomplish the goal of this example project:

* All the java classes under [core/src/main/java/com/cflinks/core](core/src/main/java/com/cflinks/core).
* Example CF component: [ui.apps/src/main/content/jcr_root/apps/cf-links/components/contentfragment](ui.apps/src/main/content/jcr_root/apps/cf-links/components/contentfragment)
* Example CF model: [ui.content/src/main/content/jcr_root/conf/cf-links/settings/dam/cfm/models/calendar-event](ui.content/src/main/content/jcr_root/conf/cf-links/settings/dam/cfm/models/calendar-event)
* Example page with a CF component instance: [ui.content/src/main/content/jcr_root/content/cf-links/us/en](ui.content/src/main/content/jcr_root/content/cf-links/us/en)
* Example CF instance: [ui.content/src/main/content/jcr_root/content/dam/cf-links/content-fragments/events/learn-with-3-share](ui.content/src/main/content/jcr_root/content/dam/cf-links/content-fragments/events/learn-with-3-share)
* Example Rewriting rule: [ui.content/src/main/content/jcr_root/etc/map/https/threeshare.cflinks.com](ui.content/src/main/content/jcr_root/etc/map/https/threeshare.cflinks.com)  
* Repository initializer for system user: [ui.config/src/main/content/jcr_root/apps/cf-links/osgiconfig/config/org.apache.sling.jcr.repoinit.RepositoryInitializer-cf-links.config](ui.config/src/main/content/jcr_root/apps/cf-links/osgiconfig/config/org.apache.sling.jcr.repoinit.RepositoryInitializer-cf-links.config)
* Service mapping for the CF Link Rewriter service: [core/src/main/resources/SLING-INF/content/org.apache.sling.serviceusermapping.impl.ServiceUserMapperImpl.amended-cfLinks.xml](core/src/main/resources/SLING-INF/content/org.apache.sling.serviceusermapping.impl.ServiceUserMapperImpl.amended-cfLinks.xml)

## How to build

To build all the modules and deploy the `all` package to a local instance of AEM, run in the project root directory the following command:

    mvn clean install -PautoInstallSinglePackage


## Testing

In order to test the CF Link Rewriter after deploying all the packages you can check here:

http://localhost:4502/content/cf-links/us/en/jcr:content/root/container/container/contentfragment.model.json

There you should see the JSON representing an example CF including the externlized URL in the _url_ property.

## Maven settings

The project comes with the auto-public repository configured. To setup the repository in your Maven settings, refer to:

    http://helpx.adobe.com/experience-manager/kb/SetUpTheAdobeMavenRepository.html
