package com.cflinks.core.utils.fuctions.impl;

import com.cflinks.core.utils.LinkUtils;
import com.cflinks.core.utils.fuctions.RewriterContentFragmentLink;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.sling.api.resource.ResourceResolver;

public class RewriterContentFragmentLinkImpl implements RewriterContentFragmentLink {

	private final ResourceResolver resourceResolver;

	public RewriterContentFragmentLinkImpl(ResourceResolver resourceResolver) {
		this.resourceResolver = resourceResolver;
	}

	@Override
	public void rewriteLink(JsonElement jsonElement) {
		JsonObject elements = jsonElement.getAsJsonObject();
		JsonObject linkObject = elements.getAsJsonObject("link");
		if (linkObject != null) {
			JsonElement linkValue = linkObject.get("value");
			if (linkValue != null) {
				String link = linkValue.getAsString();
				linkObject.addProperty("url", LinkUtils.externalizeLink(link, resourceResolver));
			}
		}
	}
}
