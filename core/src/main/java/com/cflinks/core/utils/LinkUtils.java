package com.cflinks.core.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;

public final class LinkUtils {

	private LinkUtils() {
	}

	/**
	 * Externalize page path
	 *
	 * @param path page path
	 */
	public static String externalizeLink(String path, ResourceResolver resourceResolver) {
		return resourceResolver.map(sanitizeLink(path));
	}

	/**
	 * Add .html to link if is internal
	 *
	 * @param link : link to be processed.
	 */
	public static String sanitizeLink(String link) {
		if (StringUtils.isBlank(link)) {
			return "";
		} else if (link.startsWith("/content/")) {
			return link + ".html";
		}
		return link;
	}
}
