package com.cflinks.core.utils;

import com.cflinks.core.utils.fuctions.RewriterContentFragmentLink;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.Map;
import java.util.Set;

public final class JsonUtils {

	private JsonUtils() {
	}

	public static void applyFunctionByAttribute(JsonElement element, RewriterContentFragmentLink rewriter, String attributeName) {
		if (element instanceof JsonObject) {
			Set<Map.Entry<String, JsonElement>> entries = element.getAsJsonObject().entrySet();
			for (Map.Entry<String, JsonElement> entry : entries) {
				if (entry.getKey().equals(attributeName)) {
					// apply function
					rewriter.rewriteLink(entry.getValue());
				} else {
					if (entry.getValue() instanceof JsonObject || entry.getValue() instanceof JsonArray) {
						applyFunctionByAttribute(entry.getValue(), rewriter, attributeName);
					}
				}
			}
		} else if (element instanceof JsonArray) {
			JsonArray asJsonArray = element.getAsJsonArray();
			asJsonArray.forEach(object -> {
				if (object instanceof JsonObject || object instanceof JsonArray) {
					applyFunctionByAttribute(object, rewriter, attributeName);
				}
			});
		}
	}

}
