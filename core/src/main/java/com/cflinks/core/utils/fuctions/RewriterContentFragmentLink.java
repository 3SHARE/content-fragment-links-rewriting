package com.cflinks.core.utils.fuctions;

import com.google.gson.JsonElement;

@FunctionalInterface
public interface RewriterContentFragmentLink {

	void rewriteLink(JsonElement jsonElement);

}
